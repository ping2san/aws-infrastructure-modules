resource "aws_vpc" "code_vpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "default"
  
  tags = {
    Name = "${var.vpctag}"
  }
}
