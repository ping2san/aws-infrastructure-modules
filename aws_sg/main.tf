resource "aws_security_group" "allow_port" {
  name        = "${var.sg-name}"
  description = "${var.sg-description}"
  vpc_id      = "${var.vpc_id}"

  ingress {
    # SSH (change to whatever ports you need)
    from_port   = "${var.ingrees_fromport}"
    to_port     = "${var.ingres_Toport}"
    protocol    = "${var.protocol}"
    description = "${var.sg-description}"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    }

}

